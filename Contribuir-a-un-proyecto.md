# Contribuir a un proyecto

## Fork

Este es un concepto propio de Github y no una funcionalidad de Git como se podría creer.

Un fork es sencillamente una copia de un repositorio existente pero en un cuenta de usuario o organización donde tenemos permisos de escritura. Como cabe esperar, en Github no tenemos permisos para publicar cambios en cualquier repositorio, solo aquellos donde seamos miembros o propietarios. Al tratarse de nuestra copia, podemos hacer tantos cambios como queramos.

Para poder compartir tus cambios con el repositorio original hay que añadir este último como un [git remote]. Ten en cuenta que si pretendes contribuir de manera continuada tendrás que sincronizar los cambios del original a tu fork de regularmente.

La guía de Github [Fork a repo] explica todo esto con detalles concretos para cada uno de los pasos. Ten en cuenta que tendrás que tener Git instalado en tu ordenador antes de empezar.

## Commit, push y pull

Es enorme la cantidad de recursos online para aprender git así que no entraremos en mucho detalle más que dar una breve explicación de tres conceptos básicos: commit, push y pull. Estos son los comandos de git más usaras con diferencia durante tu dia a dia.

**Commit**: Este comando se utiliza para guardar los cambios realizados a tu repositorio local.

**Push**: Se utiliza para publicar una serie de commits a un servidor remoto. Este el mecanismo para compartir tus cambios locales con el resto del equipo.

**Pull**: Descarga y integra los cambios presentes en repositorio remoto con tu repositorio local. Esto es el comando que tendrán que ejecutar tus compañeros para obtener los cambios que hayas publicado con `git push`.

### Recursos

* [Git]: documentación del propio proyecto Git
* [Learn Git Branching]: tutorial interactivo para aprender git
* [Git commands]: resumen de los comandos de git más importantes
* [Git cheat sheet de Github]: referencia de los comandos de git
* [Git syncing]: explicación muy detallada de como funcionan `git remote`, `git fetch`, `git push` y `git pull`

## Pull Request

La pull request es el mecanismo que proporciona Github para proponer cambios en un repositorio y puede ser aceptada o rechazada.

Permite mantenter un debate con el resto de la comunidad alrededor de los cambios propuestos. Esto aporta una aspecto más social al desarollo de un proyecto de programación, lo que se suele llamar _social coding_.

Una de estas prácticas sociales es el proceso de **code review**: revisar los cambios propuestos en un repositorio de forma colectiva para buscar posibles errores, transferir conocimientos, encontrar mejores soluciones y mejorar la calidad del código.

Este proceso es una pieza fundamental del desarrollo de software en general y especialmente en open source. Habitualmente el code review constituye el primer paso para integrar cambios en un proyecto.

Experiencia desde el punto de vista de contribuidor y mantenedor.

Guía de Github: [About pull requests]
Lecturas recomendadas: [Why code reviews matter (and actually save time!)], [Code Reviews: Just Do It]

### Plantillas de pull request

Muy similar a la [plantilla de issue], Github también permite definir plantillas para pull requests. El contenido de la plantilla se muestra en el campo de texto de una pull requests, lo que facilita que la persona que contribuye aporte toda la información necesaria para que los cambios se pueden integrar.

## Manos a la obra

Para afianzar los conceptos aquí explicados no hay nada mejor que hacer una contribución a un proyecto open source existente.

Busca un proyecto que te interese y navega por los issues abiertos que estén pensados para nuevos contribuidores, tales como los que tengan la etiqueta `good first issue` o parecidas.

A continuación familiarízate con las reglas y convenciones del proyecto. Puedes empezar leyendo el README, seguido de CONTRIBUTING y la wiki, si existen. Si te surgen dudas durante el proceso, no dudes en pedir ayuda en el chat o foro que el proyecto pueda tener.

Una vez clarificado como contribuir al proyecto puedes proceder a compartir tu cambio. Para ello crea tu fork y clona el repositorio en tu máquina, añade tus cambios, incluidos los tests que sean necesarios, haciendo tantos commits como creas conveniente. Cuando estés satisfecho, compártelos haciendo push a tu fork y luego abriendo una pull request.

Describe con detalle la naturaleza de tu cambio y posibles problemas que te hayas encontrado en el camino. Esto facilitará la comunicación con la mantenedora que responda a tu petición y ampliará las posibilidades que tu propuesta sea aceptada.

Sugerencia de proyecto: [Open Food Network]

[Open Food Network]: https://github.com/openfoodfoundation/openfoodnetwork/
[git remote]: https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes
[Fork a repo]: https://help.github.com/en/articles/fork-a-repo
[Git cheat sheet de Github]: https://services.github.com/on-demand/downloads/es_ES/github-git-cheat-sheet/
[Git syncing]: https://www.atlassian.com/git/tutorials/syncing
[Learn Git Branching]: https://learngitbranching.js.org/
[Git commands]: https://www.git-tower.com/learn/git/commands/
[Git]: https://git-scm.com/
[plantilla de issue]: https://gitlab.com/coopdevs/floss-communities/blob/master/Issues.md#plantilla-de-issue
[About pull requests]: https://help.github.com/en/articles/about-pull-requests
[Why code reviews matter (and actually save time!)]: https://www.atlassian.com/agile/software-development/code-reviews
[Code Reviews: Just Do It]: https://blog.codinghorror.com/code-reviews-just-do-it/
