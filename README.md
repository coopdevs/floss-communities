# FLOSS Communities

## Índice

* Antes de empezar
  * GitHub vs GitLab
  * README & CONTRIBUTING
  * Licencia
* Equipo, roles y permisos
* [Issues](https://gitlab.com/coopdevs/floss-communities/blob/master/Issues.md)
* [Contribuir a un proyecto](https://gitlab.com/coopdevs/floss-communities/blob/master/Contribuir-a-un-proyecto.md)
* [Flujos de trabajo con Git](Flujos-de-trabajo-con-git.md)
* Release
